# Puppet Terraform Integration

This repository demonstrates the integration of Puppet for configuration management with Terraform for infrastructure provisioning. It automates the setup and configuration of infrastructure resources on Azure using Terraform, followed by configuring those resources using Puppet manifests.

## Table of Contents

- [Introduction](#introduction)
- [Setup](#setup)
  - [Requirements](#requirements)
  - [Deployment](#deployment)
- [Configuration Management](#configuration-management)
- [Testing](#testing)
- [Documentation](#documentation)
- [Issues](#issues)

## Introduction

This project aims to automate the configuration management of infrastructure resources provisioned on Azure using Terraform and Puppet. The main objective is to ensure that infrastructure resources are provisioned and configured consistently and efficiently.

## Setup

### Requirements

To get started with this project, ensure you have the following tools installed:

- Terraform
- Puppet
- Azure CLI (if deploying to Azure)
- GitLab account

### Deployment

1. **Clone Repository**: Clone this repository to your local machine.

   ```bash
   git clone [repository_url]
   
2. **Terraform Setup**:

   - Update the provider block in `terraform.tf` with your Azure provider configuration.
   - Modify any other configuration variables in `terraform.tf` as needed.
   - Initialize Terraform:

     ```bash
     terraform init
   - Deploy the infrastructure using Terraform:

     ```bash
     terraform apply

3. **Puppet Setup**:

   - Write Puppet manifests to configure your resources in the `puppet/` directory.
   - Ensure that your Puppet manifests are correctly defining the desired state of your resources.

### Configuration Management

After deploying the infrastructure using Terraform, Puppet will automatically run on the provisioned instances, configuring them according to the defined Puppet manifests. Ensure that your Puppet manifests effectively configure the provisioned resources according to the desired state.

### Testing

- Validate that the provisioned resources meet the desired configuration state defined in Puppet manifests.
- Perform any necessary troubleshooting and debugging to ensure that the integration between Terraform and Puppet is functioning correctly.



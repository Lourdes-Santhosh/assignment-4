provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "example_rg" {
  name     = "example-resources"
  location = "East US"
}

resource "azurerm_linux_virtual_machine" "lourdes_instance" {
  name                  = "lourdes-instance"
  resource_group_name   = azurerm_resource_group.example_rg.name
  location              = azurerm_resource_group.example_rg.location
  size                  = "Standard_B1ls"
  admin_username        = "azureuser"
  admin_ssh_key {
    username   = "azureuser"
    public_key = file("~/.ssh/id_rsa.pub") // Change this to your public key file path
  }

  network_interface_ids = [azurerm_network_interface.lourdes_nic.id]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
}

resource "azurerm_network_interface" "lourdes_nic" {
  name                = "lourdes-nic"
  location            = azurerm_resource_group.example_rg.location
  resource_group_name = azurerm_resource_group.example_rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.lourdes_subnet.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_subnet" "lourdes_subnet" {
  name                 = "lourdes-subnet"
  resource_group_name  = azurerm_resource_group.example_rg.name
  virtual_network_name = azurerm_virtual_network.lourdes_vnet.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_virtual_network" "lourdes_vnet" {
  name                = "lourdes-network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.example_rg.location
  resource_group_name = azurerm_resource_group.example_rg.name
}
